/*
*  Copyright (c) 2018, ����� ����������� ����������� (�����, ������� ��� ���)
*  Copyright (c) 2018, ������� �.�., ����1-7
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "requests.h"

int main()
{
	// ������ ������ � ��������.
	short int roomsNumber; 
	room masRooms[NMAX_ROOMS]; 

	// ������� ������ � ��������.
	readRooms(&roomsNumber, masRooms);

	// ��������������� ������� �� ���� �������.
	sortRooms(roomsNumber, masRooms);

	// ������� ������� ��������� ������ � ����.
	createOccupancyMatrix(roomsNumber, masRooms);

	// ���������� �������.
	processRequests(roomsNumber, masRooms);

	// ���������� ������� ��������� ������.
	freeOccupancyMatrix(roomsNumber, masRooms);

	return 0;
}