/*
*  Copyright (c) 2018, ����� ����������� ����������� (�����, ������� ��� ���)
*  Copyright (c) 2018, ������� �.�., ����1-7
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "requests.h"

// ������������ ������� ���� GET ROOM.
void processGetRoomRequest(FILE* fout, request *request_ptr, short int roomsNumber, room* masRooms)
{
	// �������� �� ��, ���� �� ������� ������� �������.
	short int flagRoomFound = 0;
	for (int i = 0; i < roomsNumber; i++)
	{
		// ���� ���������� ������� �������, ����������� ����� ������ � �������.
		if (masRooms[i].id == request_ptr->id)
		{
			fprintf(fout, "ROOM %hd: CAPACITY %hd, %hd BATHROOMS, %hd ROUBLES.\n",
				masRooms[i].id, masRooms[i].capacity, masRooms[i].bathnum,
				masRooms[i].cost);

			flagRoomFound = 1;
			break;
		}
	}

	// ���� ������� ������� �� ���� �������, ����� ���������������� ���������.
	if (!flagRoomFound)
		fprintf(fout, "THERE IS NO ROOM WITH ID = %hd.\n", request_ptr->id);

	return;
}

// ������������ ������� ���� ADD_CLIENT.
void processAddClientRequest(FILE* fout, request *request_ptr, short int roomsNumber, room* masRooms)
{
	// ������ �������, ��������� ��� ������������.
	short int roomIndex = 0; 
	// �������� �� ��, ������� �� ������������� �������.
	short int flagRoomBooked = 0; 

	// ����� ������� � ������ ������� �������.
	// ��� ��� ������� ��� ������������� �� �������, ������� ������ �� ������ � ���������� ��������.
	while ((roomIndex < roomsNumber) && (request_ptr->bathnum != masRooms[roomIndex].bathnum))
		roomIndex++;
		
	// ������������ ���������� ��������������� �������� � ���������� ������� �������.
	short int maxCurrentOccupancy;

	// ���� �������� �� ���� �������� � ������ ����� �������.
	// ���� �� ������������ ������� � ������ �������� (�������� ������������� ���-�� ����), 
	// �� ������ ������ � ������ �������� �� ��������.
	while ((!flagRoomBooked) && (roomIndex < roomsNumber)
		&& (request_ptr->bathnum == masRooms[roomIndex].bathnum))
	{
		maxCurrentOccupancy = findMaxOccupancy(request_ptr, roomsNumber, masRooms, roomIndex);
		// ���� � ������� ���������� ��� ���� ������� �� ��� ������ ����������, �� ��������� �����.
		if (maxCurrentOccupancy < masRooms[roomIndex].capacity)
		{
			flagRoomBooked = 1;

			// ������������ ����� ����� ���������� ����� ������� �������� � �������� ���������� �������.
			// ���� �� (����_������ - 1) ������, ��� ������ "i"-�� "����" ���������� ������������ ����� �����
			// � 12:00 "i"-�� ��� �� 12:00 "i+1"-�� ���. 
			// ������� �� �� �������� � ������������ �����, ������������ � ���� ������.
			for (int i = request_ptr->date1; i < request_ptr->date2; i++)
			{
				masRooms[roomIndex].masOccupancy[i]++;
			}

			fprintf(fout, "THE ROOM %hd IS BOOKED FOR %hd ROUBLES PER DAY.\n",
				masRooms[roomIndex].id, masRooms[roomIndex].cost);
		}
		// ���� ��������� ������� �� �������� ��� ������������, ��������� � ��������� �������.
		roomIndex++;
	}

	// ���� ������������� ������� �� �������, ����� ���������������� ���������.
	if (!flagRoomBooked)
		fprintf(fout, "NO VACANT BEDS IN SERVICE = %s.\n", request_ptr->service);

	return;
}

// ����� � ���������� ������� ����������� ����� ������� ��������.
short int findMaxOccupancy(request *request_ptr, short int roomsNumber, room* masRooms, short int roomIndex)
{
	short int maxCurrentOccupancy = 0;
	for (int i = request_ptr->date1; i < request_ptr->date2; i++)
	{
		if (masRooms[roomIndex].masOccupancy[i] > maxCurrentOccupancy)
			maxCurrentOccupancy = masRooms[roomIndex].masOccupancy[i];
	}
	return maxCurrentOccupancy;
}

/*
 ��������� �� ���� ��� �����: ���� � ����� �� ���� ���� "��:��". 
 ���������� �����, ��������������� ������ ��� � ����.
*/
int calculateDate(short int day, short int month)
{
	// ������ ���������� ���� � ������ ������
	short int masMonths[12]
		= { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	int resDate = day;
	// ����� ���������� ������ ������ ������, �������� ����� ������� �� 1. 
	month--;
	for (int i = 0; i < month; i++)
		resDate += masMonths[i];

	return resDate;
}


/*
 �� ���� �������� ���������� ������ � ������ ������.
 �������� �� ��������� ��������.
*/
void processRequests(short int roomsNumber, room* masRooms)
{
	FILE *finRequest = fopen("input.txt", "rt");
	short int requestsNumber;
	fscanf(finRequest, "%hd\n", &requestsNumber);
	// ������ �������������� � ������ ������ ������.
	request requestCurrent; 

	FILE* fout = fopen("output.txt", "wt");

	// �������� ���� ���������� �������� �� ��� �������. 1 - GET ROOM, 2 - ADD CLIENT.
	int requestClass;
	for (int i = 0; i < requestsNumber; i++)
	{
		// ����������� ������, ������������ ��� ���.
		requestClass = readIdentifyRequest(finRequest, &requestCurrent);

		// ��������� ������� ����������� ����.
		switch (requestClass)
		{
			case GET_ROOM:
			{
				processGetRoomRequest(fout, &requestCurrent, roomsNumber, masRooms);
				break;
			}
			case ADD_CLIENT:
			{
				processAddClientRequest(fout, &requestCurrent, roomsNumber, masRooms);
				break;
			}
		}
	}

	// ��������� �����.
	fclose(finRequest);
	fclose(fout);

	return;
}

/*
 ��������� ������ � ������� ������� � ���������. 
 ���������� �����, ��������������� ���� ���������� �������.
*/
int readIdentifyRequest(FILE *fin, request *request_ptr)
{
	/*
	� ��� ����������� ���������� � ������, ��� ��������� ���������� ��� �������.
	�������� ��� ��������� ���������� �������, ��� ��� � ��������� �������� �� ������������.
	*/
	char instruction[INSTRUCTION_LENGTH];
	char object[OBJECT_LENGTH];

	// �������� �� ���, �������������� ���� �������.
	int requestType = 0;

	fscanf(fin, "%s %s ", &instruction, &object);

	// �� ������ ����� ����� ���������� ����������.
	switch (instruction[0])
	{
		// ������ GET...
		case 'G':
		{
			// ����������� ������� GET ROOM �� ������ ����� �������.
			switch (object[0])
			{
				// ������ GET ROOM.
				case 'R':
				{
					fscanf(fin, "ID = %hd\n", &request_ptr->id);
					requestType = GET_ROOM;
					break;
				}
			}
			break;
		};

		// ������ ADD CLIENT.
		case 'A':
		{
			// ��������� ����������, ��������� ���� �� � �������������� � ���� �����.
			short int day1, month1, day2, month2;

			fscanf(fin,
				"SERVICE = %s BEGIN_DATE = %hd.%hd.2020 END_DATE = %hd.%hd.2020\n",
				&request_ptr->service, &day1, &month1, &day2, &month2);

			// ���������� ��� ������ � ������.
			request_ptr->date1 = calculateDate(day1, month1);
			request_ptr->date2 = calculateDate(day2, month2);

			// �������������� ������ SERVICE � �����, ��������������� ����� ���� � �������.
			request_ptr->bathnum = serviceToBathnum(request_ptr->service);

			requestType = ADD_CLIENT;
			break;

		};
	}
	return requestType;
}

