/*
*  Copyright (c) 2018, ����� ����������� ����������� (�����, ������� ��� ���)
*  Copyright (c) 2018, ������� �.�., ����1-7
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "rooms.h"

// ������� ��������� ������ � �������� � ������ ��������.
void readRooms(short int* numb_ptr, room* masRooms)
{
	FILE* finRooms = fopen("rooms.txt", "rt");

	fscanf(finRooms, "%hd\n", numb_ptr);
	// ���������� ������ ������ � �������.
	for (int i = 0; i < *numb_ptr; i++)
	{
		fscanf(finRooms, "%hd %s %hd %hd\n", &masRooms[i].id, &masRooms[i].service,
			&masRooms[i].capacity, &masRooms[i].cost);
		masRooms[i].bathnum = serviceToBathnum(masRooms[i].service);
	}

	fclose(finRooms);

	return;
}

/*
��� ������ ������� ������ ������ ���������� ������� ���� � ������������ �����.
������� ���������� ����������� ��� �������� ����, ��� ���� �� ������������.
*/
void createOccupancyMatrix(short int roomsNumber, room* masRooms)
{
	for (int i = 0; i < roomsNumber; i++)
		masRooms[i].masOccupancy = (byte*)calloc(INYEAR_DAYS, sizeof(byte));
}

// ����������� ������ �� ��������� ��� ������ ������� �������� ��������� ����.
void freeOccupancyMatrix(short int roomsNumber, room* masRooms)
{
	for (int i = 0; i < roomsNumber; i++)
	{
		free(masRooms[i].masOccupancy);
		masRooms[i].masOccupancy = NULL;
	}
}

// �������� �� ���� ������-�����, ���������� �� ��� �������. ���������� ����� ���� � �������.
short int serviceToBathnum(char* service)
{
	switch (service[0])
	{
		case 'E':
		{
			return BATHNUM_ECONOMY;
			break;
		}
		case 'S':
		{
			return BATHNUM_STANDART;
			break;
		}
		case 'C':
		{
			return BATHNUM_COMFORT;
			break;
		}
	}
}

// ���������� ������ �� ���������� ���� � ������� ������� ���������. 
void sortRooms(short int roomsNumber, room* masRooms)
{
	room temp;
	// ���� ������ ����� �������, ��������� ���������� ���������.
	if (roomsNumber > 1)
		for (int i = 0; i < roomsNumber - 1; i++)
			for (int j = 0; j < roomsNumber - i - 1; j++)
			{
				if (masRooms[j].bathnum > masRooms[j + 1].bathnum)
				{
					temp = masRooms[j];
					masRooms[j] = masRooms[j + 1];
					masRooms[j + 1] = temp;
				}
			}
	return;
}