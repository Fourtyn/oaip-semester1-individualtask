/*
*  Copyright (c) 2018, ����� ����������� ����������� (�����, ������� ��� ���)
*  Copyright (c) 2018, ������� �.�., ����1-7
*
* ����������� �������������� ������������� � ��������������� ������� ������ �
* ���� ������ � ������������ ������
*
* ����������: ������ �������������� � ����������������
* �������������� �������
*
* ��������� � ���� ������, ���������� �� ������ � ��������� (���� ������, ����������� � input.txt)
* � ������� ��� ������ � ����.
* 
* ���������� ��������:
* - ������ ������� � ���������
* - ������ ����������� ���������� �����, ���������� ������� � ������������ ���������� �������;
* - �������������� ���� � ����� ��� � ����;
* - ��������� �������� ���� GET ROOM � ADD CLIENT;
* 
* 27.12.2018.
*/

#pragma once

#include "rooms.h"

// ������������ �����, ���������� �� ��� �������.
enum RequestClass
{
	GET_ROOM = 1,
	ADD_CLIENT = 2
};

// ��������� ��� �������� ������ � �������
typedef struct
{
	// ������������� ����������� ����������.
	short int id;
	char service[SERVICE_LENGTH];

	// ������ ���������� ������. � ����������� ��������, ��������� � ��������, 
	// ������������ ������ ���� SERVICE ��� ��������.
	short int bathnum; 
	// ������������� ��������� BEGIN_DATE.
	short int date1; 
	// ������������� ��������� END_DATE.
	short int date2; 
} request;

//--------------------------------------------

// ������������ ������� ���� GET ROOM.
void processGetRoomRequest(FILE* fout, request *request_ptr, short int roomsNumber, room* masRooms);

// ������������ ������� ���� ADD_CLIENT.
void processAddClientRequest(FILE* fout, request *request_ptr, short int roomsNumber, room* masRooms);

// ����� � ���������� ������� ����������� ����� ������� ��������.
short int findMaxOccupancy(request *request_ptr, short int roomsNumber, room* masRooms, short int roomIndex);

/*
��������� �� ���� ��� �����: ���� � ����� �� ���� ���� "��:��".
���������� �����, ��������������� ������ ��� � ����.
*/
int calculateDate(short int day, short int month);

/*
�� ���� �������� ���������� ������ � ������ ������.
�������� �� ��������� ��������.
*/
void processRequests(short int roomsNumber, room* masRooms);

/*
��������� ������ � ������� ������� � ���������.
���������� �����, ��������������� ���� ���������� �������.
*/
int readIdentifyRequest(FILE *fin, request *request_ptr);

